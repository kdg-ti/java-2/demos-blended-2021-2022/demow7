import be.kdg.model.Piloot;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ParseStaxXml {
    private static final String FILENAME = "data/piloten.xml";

    public static void main(String[] args) {
        boolean naamElementGevonden = false;
        boolean nummerElementGevonden = false;
        boolean wedstrijdElementGevonden = false;
        List<Piloot> piloten = new ArrayList<>();

        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(FILENAME));

            Piloot piloot = null;
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                if (event.getEventType() == XMLStreamConstants.START_ELEMENT) { //Als een start-tag gevonden werd
                    StartElement startElement = event.asStartElement();
                    String tagName = startElement.getName().getLocalPart();
                    switch (tagName) {
                        case "piloot":
                            piloot = new Piloot();
                            Iterator<Attribute> attributes = startElement.getAttributes();
                            piloot.setTeam(attributes.next().getValue());
                            break;
                        case "naam":
                            naamElementGevonden = true;
                            break;
                        case "nummer":
                            nummerElementGevonden = true;
                            break;
                        case "wedstrijd":
                            wedstrijdElementGevonden = true;
                            break;
                    }
                } else if (event.getEventType() == XMLStreamConstants.CHARACTERS) { //Als we content gevonden hebben
                    Characters characters = event.asCharacters();
                    if (naamElementGevonden) {
                        piloot.setNaam(characters.getData());
                        naamElementGevonden = false;
                    }
                    if (nummerElementGevonden) {
                        piloot.setNummer(Integer.parseInt(characters.getData()));
                        nummerElementGevonden = false;
                    }
                    if (wedstrijdElementGevonden) {
                        piloot.setWedstrijdDatum(LocalDate.parse(characters.getData()));
                        wedstrijdElementGevonden = false;
                    }
                } else if (event.getEventType() == XMLStreamConstants.END_ELEMENT) { //Als we een end-tag gevonden hebben
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals("piloot")) {
                        // end-tag gelezen, dus piloot is volledig en kan aan List worden toegevoegd
                        piloten.add(piloot);
                    }
                }
            }
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }

        System.out.println("Overzicht piloten:\n");
        piloten.forEach(System.out::println);

    }
}

